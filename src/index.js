const csvToJson = require('convert-csv-to-json').fieldDelimiter(',')
const fs = require('fs')
const tableify = require('tableify')

const C = require('./constants')

const initCreateHTMLReport = require('./utils/createHTMLReport')

const writer = fs.createWriteStream(C.outputFilePath)
  .on('error', console.log)

const createHTMLReport = initCreateHTMLReport(C, tableify, writer)

const json = csvToJson.getJsonFromCsv(C.inputFilePath)

createHTMLReport(json)