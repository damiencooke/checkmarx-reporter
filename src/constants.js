module.exports = {
  HTMLHead: '<!DOCTYPE html><html><header></header><body>',
  HTMLTail: '</body></html>',
  inputFilePath: process.env.INPUT_FILE_PATH ? process.env.INPUT_FILE_PATH : 'checkmarx.csv',
  outputFilePath: process.env.OUTPUT_FILE_PATH ? process.env.OUTPUT_FILE_PATH : 'output/htmlReport.html'
}