module.exports = (C, tableify, writer) => 
  json => {
    const html = C.HTMLHead + tableify(json) + C.HTMLTail
    writer.write(html)
}